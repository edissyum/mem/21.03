<?php

/**
* Copyright Maarch since 2008 under licence GPLv3.
* See LICENCE.txt file at the root folder for more details.
* This file is part of Maarch software.
*
*/

/**
* @brief Watermark Controller
* @author dev@maarch.org
*/

namespace Resource\controllers;

use Attachment\models\AttachmentModel;
use Configuration\models\ConfigurationModel;
use Convert\controllers\ConvertPdfController;
use Docserver\controllers\DocserverController;
use Docserver\models\DocserverModel;
use Resource\models\ResModel;
use setasign\Fpdi\Tcpdf\Fpdi;
use SrcCore\models\CoreConfigModel;
use SrcCore\models\ValidatorModel;

// EDISSYUM - NCH01 Ne pas afficher le watermark sur les documents signés par un parapheur externe
function isStringInFile($file, $array_string): bool {
    $handle = fopen($file, 'r');
    $valid = false;
    while (($buffer = fgets($handle)) !== false) {
        foreach ($array_string as $string) {
            if (strpos(strtolower($buffer), strtolower($string)) !== false) {
                $valid = TRUE;
                break;
            }
        }
    }
    fclose($handle);
    return $valid;
}
// END EDISSYUM - NCH01

class WatermarkController
{
    public static function watermarkResource(array $args)
    {
        ValidatorModel::notEmpty($args, ['resId', 'path']);
        ValidatorModel::intVal($args, ['resId']);
        ValidatorModel::stringType($args, ['path']);

        $configuration = ConfigurationModel::getByPrivilege(['select' => ['value'], 'privilege' => 'admin_parameters_watermark']);
        if (empty($configuration)) {
            return null;
        }

        // EDISSYUM - NCH01 Ne pas afficher le watermark sur les documents signés par un parapheur externe
        if (isStringInFile($args['path'], ['sign', 'Sunnystamp'])) {
            return null;
        }
        // END EDISSYUM - NCH01

        $watermark = json_decode($configuration['value'], true);
        if ($watermark['enabled'] != 'true') {
            return null;
        } elseif (empty($watermark['text'])) {
            return null;
        }

        $text = $watermark['text'];
        preg_match_all('/\[(.*?)\]/i', $watermark['text'], $matches);
        foreach ($matches[1] as $value) {
            if ($value == 'date_now') {
                $tmp = date('d-m-Y');
            } elseif ($value == 'hour_now') {
                $tmp = date('H:i');
            } else {
                $resource = ResModel::getById(['select' => [$value], 'resId' => $args['resId']]);
                $tmp = $resource[$value] ?? '';
            }
            $text = str_replace("[{$value}]", $tmp, $text);
        }

        $libDir = CoreConfigModel::getLibrariesDirectory();
        if (!empty($libDir) && is_file($libDir . 'SetaPDF-FormFiller-Full/library/SetaPDF/Autoload.php')) {
            require_once($libDir . 'SetaPDF-FormFiller-Full/library/SetaPDF/Autoload.php');

            $flattenedFile = CoreConfigModel::getTmpPath() . "tmp_file_{$GLOBALS['id']}_" .rand(). "_watermark.pdf";
            $writer = new \SetaPDF_Core_Writer_File($flattenedFile);
            $document = \SetaPDF_Core_Document::loadByFilename($args['path'], $writer);

            $formFiller = new \SetaPDF_FormFiller($document);
            $fields = $formFiller->getFields();
            $fields->flatten();
            $document->save()->finish();

            $args['path'] = $flattenedFile;
        }

        // EDISSYUM - NCH01 Amélioration de la gestion du watermark, rajout d'un bouton pour télécharger le fichier original
        $extension = pathinfo($args['path'], PATHINFO_EXTENSION);
        if ($extension != 'pdf') {
            $converted_file = ConvertPdfController::getConvertedPdfById(['resId' => $args['resId'], 'collId' => 'letterbox_coll']);
            if (!empty($converted_file)) {
                $docserver = DocserverModel::getByDocserverId(['docserverId' => $converted_file['docserver_id']]);
                $args['path'] = $docserver['path_template'] . $converted_file['path'] . $converted_file['filename'];
            }
        }
        // END EDISSYUM - NCH01

        try {
//            $pdf = new Fpdi('P', 'pt'); // EDISSYUM - NCH01 Correction des URLS dans les pdf si le watermark est activé | Commenter cette ligne
            $pdf = new TcpdfFpdiCustom('P', 'pt'); // EDISSYUM - NCH01 Correction des URLS dans les pdf si le watermark est activé
            $nbPages = $pdf->setSourceFile($args['path']);
            $pdf->setPrintHeader(false);
            for ($i = 1; $i <= $nbPages; $i++) {
                $page = $pdf->importPage($i, 'CropBox');
                $size = $pdf->getTemplateSize($page);
                $pdf->AddPage($size['orientation'], $size);
//                $pdf->useImportedPage($page); // EDISSYUM - NCH01 Correction des URLS dans les pdf si le watermark est activé | Commenter cette ligne
                $pdf->useTemplate($page); // EDISSYUM - NCH01 Correction des URLS dans les pdf si le watermark est activé
                $pdf->SetFont($watermark['font'], '', $watermark['size']);
                $pdf->SetTextColor($watermark['color'][0], $watermark['color'][1], $watermark['color'][2]);
                $pdf->SetAlpha($watermark['opacity']);
                $pdf->Rotate($watermark['angle']);
                $pdf->Text($watermark['posX'], $watermark['posY'], $text);
            }
            $fileContent = $pdf->Output('', 'S');
        } catch (\Exception $e) {
            //$fileContent = null; EDISSYUM - NCH01 Amélioration du watermark si la compression n'est pas gérée par FPDI | Commenter cette ligne

            // EDISSYUM - NCH01 Amélioration du watermark si la compression n'est pas gérée par FPDI
            if (isset($args['second']) && $args['second']) {
                $fileContent = null;
            } else {
                $flattenedFile = CoreConfigModel::getTmpPath() . "tmp_file_{$GLOBALS['id']}_" .rand(). "_watermark.pdf";
                $command = "gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dNOPAUSE -dQUIET -dBATCH -sOutputFile={$flattenedFile} {$args['path']}";
                exec($command, $output, $return);
                return self::watermarkResource(['resId' => $args['resId'], 'path' => $flattenedFile, 'second' => true]);
            }
            // END EDISSYUM - NCH01
        }

        if (!empty($flattenedFile) && is_file($flattenedFile)) {
            unlink($flattenedFile);
        }

        return $fileContent;
    }

    /**
     * @codeCoverageIgnore
     */
    public static function watermarkAttachment(array $args)
    {
        ValidatorModel::notEmpty($args, ['attachmentId', 'path']);
        ValidatorModel::intVal($args, ['attachmentId']);
        ValidatorModel::stringType($args, ['path']);

        $loadedXml = CoreConfigModel::getXmlLoaded(['path' => 'modules/attachments/xml/config.xml']);
        if (empty($loadedXml)) {
            return null;
        }

        $watermark = (array)$loadedXml->CONFIG->watermark;
        if ($watermark['enabled'] != 'true') {
            return null;
        } elseif (empty($watermark['text'])) {
            return null;
        }

        $text = $watermark['text'];
        preg_match_all('/\[(.*?)\]/i', $watermark['text'], $matches);
        foreach ($matches[1] as $value) {
            if ($value == 'date_now') {
                $tmp = date('d-m-Y');
            } elseif ($value == 'hour_now') {
                $tmp = date('H:i');
            } else {
                $attachment = AttachmentModel::getById(['select' => [$value], 'id' => $args['attachmentId']]);
                $tmp = $attachment[$value] ?? '';
            }
            $text = str_replace("[{$value}]", $tmp, $text);
        }

        $color = ['192', '192', '192']; //RGB
        if (!empty($watermark['text_color'])) {
            $rawColor = explode(',', $watermark['text_color']);
            $color = count($rawColor) == 3 ? $rawColor : $color;
        }

        $font = ['helvetica', '10']; //Familly Size
        if (!empty($watermark['font'])) {
            $rawFont = explode(',', $watermark['font']);
            $font = count($rawFont) == 2 ? $rawFont : $font;
        }

        $position = [30, 35, 0, 0.5]; //X Y Angle Opacity
        if (!empty($watermark['position'])) {
            $rawPosition = explode(',', $watermark['position']);
            $position = count($rawPosition) == 4 ? $rawPosition : $position;
        }

        $libDir = CoreConfigModel::getLibrariesDirectory();
        if (!empty($libDir) && is_file($libDir . 'SetaPDF-FormFiller-Full/library/SetaPDF/Autoload.php')) {
            require_once($libDir . 'SetaPDF-FormFiller-Full/library/SetaPDF/Autoload.php');

            $flattenedFile = CoreConfigModel::getTmpPath() . "tmp_file_{$GLOBALS['id']}_" .rand(). "_watermark.pdf";
            $writer = new \SetaPDF_Core_Writer_File($flattenedFile);
            $document = \SetaPDF_Core_Document::loadByFilename($args['path'], $writer);

            $formFiller = new \SetaPDF_FormFiller($document);
            $fields = $formFiller->getFields();
            $fields->flatten();
            $document->save()->finish();

            $args['path'] = $flattenedFile;
        }

        try {
            $pdf = new Fpdi('P', 'pt');
            $nbPages = $pdf->setSourceFile($args['path']);
            $pdf->setPrintHeader(false);
            for ($i = 1; $i <= $nbPages; $i++) {
                $page = $pdf->importPage($i, 'CropBox');
                $size = $pdf->getTemplateSize($page);
                $pdf->AddPage($size['orientation'], $size);
                $pdf->useImportedPage($page);
                $pdf->SetFont($font[0], '', $font[1]);
                $pdf->SetTextColor($color[0], $color[1], $color[2]);
                $pdf->SetAlpha($position[3]);
                $pdf->Rotate($position[2]);
                $pdf->Text($position[0], $position[1], $text);
            }
            $fileContent = $pdf->Output('', 'S');
        } catch (\Exception $e) {
            $fileContent = null;
        }

        return $fileContent;
    }
}
