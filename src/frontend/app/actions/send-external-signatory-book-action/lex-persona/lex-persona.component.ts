import {Component, ElementRef, EventEmitter, Inject, Input, OnInit, Output, ViewChild} from '@angular/core';
import {PrivilegeService} from '@service/privileges.service';
import {FunctionsService} from '@service/functions.service';
import {HeaderService} from '@service/header.service';
import {catchError, exhaustMap, filter, finalize, map, startWith, tap} from 'rxjs/operators';
import {Observable, of} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import {HttpClient} from '@angular/common/http';
import {FormControl} from '@angular/forms';
import {NotificationService} from '@service/notification/notification.service';
import {LatinisePipe, ScanPipe} from 'ngx-pipes';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {ActivatedRoute} from '@angular/router';
@Component({
    selector: 'app-lex-persona',
    templateUrl: './lex-persona.component.html',
    styleUrls: ['./lex-persona.component.scss'],
    providers: [ScanPipe]
})
export class LexPersonaComponent implements OnInit {
    @Input() additionalsInfos: any;
    @Input() externalSignatoryBookDatas: any;
    @Input() injectDatas: any;
    @Input() target: string = '';
    @Input() adminMode: boolean;
    @Input() resId: number = null;

    @Input() showListModels: boolean = true;
    @Input() showComment: boolean = true;
    @Output() workflowUpdated = new EventEmitter<any>();
    @ViewChild('searchVisaSignUserInput', { static: false }) searchVisaSignUserInput: ElementRef;
    visaWorkflow: any = {
        roles: ['sign', 'visa'],
        selectedRole: '',
        selectedOrganizationId: '',
        items: []
    };

    sendedInfos: { external_id: any, selectedRole: string, selectedOrganizationId: string, item_mode: string, item_id: number }[] = [];
    visaWorkflowClone: any = [];
    visaTemplates: any = {
        private: [],
        public: []
    };
    signVisaUsers: any = [];
    filteredSignVisaUsers: Observable<string[]>;
    filteredPublicModels: Observable<string[]>;
    filteredPrivateModels: Observable<string[]>;

    selectedRoles: { [key: number]: string } = {};
    loading: boolean = false;
    hasHistory: boolean = false;
    visaModelListNotLoaded: boolean = true;
    // data: any;

    searchVisaSignUser = new FormControl();

    loadedInConstructor: boolean = false;
    workflowSignatoryRole: string;
    errorMessage: string = '';
    constructor(
        public translate: TranslateService,
        public http: HttpClient,
        private notify: NotificationService,
        public functions: FunctionsService,
        private latinisePipe: LatinisePipe,
        @Inject(MAT_DIALOG_DATA) public data: any,
        public dialog: MatDialog,
        private scanPipe: ScanPipe,
        private route: ActivatedRoute,
        private privilegeService: PrivilegeService,
        public headerService: HeaderService
    ) {}
    ngOnInit(): void {
        this.adminMode = true;
        this.resId = this.data['resource']['resId'];
        this.loadWorkflowWithRoles(this.resId);
        console.log('==> ' , this.visaWorkflow.items);
    }
    selectRole(index: number, title: string) {
        this.selectedRoles[index] = title;
        this.visaWorkflow.items[index]['selectedRole'] = title;
        this.visaWorkflow.items.forEach((element: any) => {
            element['roles'].forEach((secondElement: any) => {
                if ( secondElement.title === title ) {
                    this.visaWorkflow.items[index]['selectedOrganizationId'] = secondElement['organizationId'];
                }
            });
        });
        console.log(title);
    }
    getRessources() {
        return this.additionalsInfos.attachments.map((e: any) => e.res_id);
    }
    getDatas() {
        this.sendedInfos.splice(0);
        this.visaWorkflow.items.map(({external_id, selectedRole, selectedOrganizationId, item_mode, item_id}) => {
            this.sendedInfos.push({external_id, selectedRole, selectedOrganizationId, item_mode, item_id});
        });
        this.externalSignatoryBookDatas = {
            'lexPersona': this.sendedInfos,
            'actionId': this.data['action']['id'],
            'steps': []
        };
        return this.externalSignatoryBookDatas;
    }
    isValidParaph() {
        if (this.visaWorkflow.items.length === 0) {
            return false;
        }
        return true;
    }
    getLastVisaUser() {
        const arrOnlyProcess = this.visaWorkflow.items.filter((item: any) => !this.functions.empty(item.process_date) && item.isValid);
        return !this.functions.empty(arrOnlyProcess[arrOnlyProcess.length - 1]) ? arrOnlyProcess[arrOnlyProcess.length - 1] : '';
    }

    getRealIndex(index: number) {
        while (index < this.visaWorkflow.items.length && !this.visaWorkflow.items[index].isValid) {
            index++;
        }
        return index;
    }

    getCurrentVisaUserIndex() {
        if (this.getLastVisaUser().listinstance_id === undefined) {
            const index = 0;
            return this.getRealIndex(index);
        } else {
            let index = this.visaWorkflow.items.map((item: any) => item.listinstance_id).indexOf(this.getLastVisaUser().listinstance_id);
            index++;
            return this.getRealIndex(index);
        }
    }
    loadWorkflowWithRoles(resId: number) {
        this.resId = resId;
        this.loading = true;
        this.visaWorkflow.items = [];
        return new Promise((resolve) => {
            this.http.get(`../rest/lexPersona/getUser/${this.resId}?actionId=${this.data['action']['id']}`).pipe(
                tap((data: any) => {
                    if (!this.functions.empty(data.circuit)) {
                        data.circuit.forEach((element: any) => {
                            this.visaWorkflow.items.push(
                                {
                                    ...element,
                                    difflist_type: 'VISA_CIRCUIT',
                                    currentRole: this.getCurrentRole(element)
                                });
                        });

                        this.visaWorkflowClone = JSON.parse(JSON.stringify(this.visaWorkflow.items));
                    }
                    this.hasHistory = data.hasHistory;
                }),
                finalize(() => {
                    this.loading = false;
                    this.visaWorkflow.items.forEach((element: any) => {
                        element['roles'].push({
                            title: 'Aucune fonction',
                            organizationId: 'Aucune fonction'
                        });
                    });
                    resolve(true);
                }),
                catchError((err: any) => {
                    this.errorMessage = err.error.errors;
                    return of(false);
                })
            ).subscribe();
        });
    }
    stringIncludes(source, search) {
        if (source === undefined || source === null) {
            return false;
        }

        return source.includes(search);
    }
    getCurrentRole(item: any) {
        if (this.functions.empty(item.process_date)) {
            return item.requested_signature ? 'sign' : 'visa';
        } else {
            if (this.stringIncludes(item.process_comment, this.translate.instant('lang.visaWorkflowInterrupted'))) {
                return item.requested_signature ? 'sign' : 'visa';
            } else {
                return item.signatory ? 'sign' : 'visa';
            }
        }
    }
}
